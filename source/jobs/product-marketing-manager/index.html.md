---
layout: job_page
title: "Director of Product Marketing"
---
The Product Marketing Manager role includes 4 key areas of focus:

* Monthly release management
* Product messaging
* Sales enablement
* Market research

## Responsibilities

* Manage monthly release post with key understanding of technical details and ability to gather input from many people.
* Create sales enablement materials like white-papers and case studies.
* Conduct market research to understand buyer personas, competitive landscape, and other data to help influence marketing, sales, and product decisions.
* Work with product and engineering to write feature highlights in addition to the monthly release post.
* Help technical writing and engineering to write documentation.
* Create positioning and copy for the website. Make sure the website is updated in all appropriate areas at release time.
* Work with engineering and design on creating new ways to market products or releases. For example, infographics or animations explaining complex new features.
* Manage expectations and timelines about upcoming features.

## Requirements

* Technical background or clear understanding of developer products.
* 3-5 years of experience in developer product marketing.
* Able to coordinate across many teams and perform in fast-moving startup environment.
* Excellent spoken and written English
* You share our [values](/handbook/#values), and work in accordance with those values.
