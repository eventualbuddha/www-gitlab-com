---
layout: job_page
title: "Global Recruiter"
---

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy), everyone can contribute to all digital content and our mission is to change all creative work from read-only to read-write so that everyone can contribute.  

We [value](https://about.gitlab.com/handbook/#values) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer) to learn more. Open source is our culture, our way of life, our story and what makes us truly unique.

[Top 10 reasons](https://about.gitlab.com/culture/) to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focussed on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.


## Responsibilities

* Collaborate with hiring managers to understand their requirements and set a proper recruiting strategy
* Develop accurate job descriptions to attract a qualified candidate pool
* Find creative and strategic ways to source top talent
* Promote our remote only culture
* Apply effective recruiting practices to passive and active candidates
* Screen, interview and evaluate candidates
* Assess candidate interest and ability to thrive in an open source culture
* Foster lasting relationships with candidates
* Build an effective network of internal and external resources to call on as needed
* Share best practice interviewing techniques with management team
* Ensure applicant tracking system is maintained and applicants receive timely responses
* Partner with Marketing to develop our Employer Brand in the marketplace


## Requirements

* 3+ years experience recruiting for technical roles, preferably in a technical company
* Experience with global job markets highly preferred
* Comfortable competing for top talent in a competitive global market
* Focused on delivering an excellent candidate experience
* Ambitious, efficient and stable under tight deadlines and competing priorities
* Advanced working knowledge using applicant tracking systems effectively
* Outstanding communication across all levels
* College degree from an accredited institution preferred, equivalent work experience considered
