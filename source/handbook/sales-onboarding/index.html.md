---
layout: markdown_page
title: "Sales Onboarding"
---

Every salesperson who starts at GitLab will enter an intense 30-day sales bootcamp.  The weekly class schedule can be found at [GitLab University](https://university.gitlab.com/).  In addition, once a new salesperson starts, their manager will create an issue for each of the first 4 weeks, tracking the progress of the new hire.

```
### Stage 1

**Familiarize yourself with the basics**

* [ ] Understanding [terminology](http://university.gitlab.com/glossary/).
   Please create a merge request for any term that does not have an answer or
   to add a term that you feel is relevant but is not on this page.

* [ ] What is Git
  * [ ] [Basics of software version control](https://git-scm.com/video/what-is-version-control) (6 minutes)
  * [ ] [The evolution of SVC products](http://blog.plasticscm.com/2010/11/version-control-timeline.html)
  * [ ] [Intro to Git](https://www.youtube.com/watch?v=3m7BgIvC-uQ) (1:39 hours)

* [ ] Application Lifecycle Management (ALM):
  * [ ] [What is ALM?](https://blogs.msdn.microsoft.com/africaapps/2013/05/29/application-lifecycle-management-part-1-of-5/)
  * [ ] [5 Imperatives for ALM](https://www.youtube.com/watch?v=l_W5nkG6iAE) (3 minutes)
  * [ ] [The ecosystem](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6) (1:03 hours)
        - [Slides](https://docs.google.com/presentation/d/1vCU-NbZWz8NTNK8Vu3y4zGMAHb5DpC8PE5mHtw1PWfI/edit)
  * [ ] [Development methodologies (waterfall, agile, scrum, kanban etc.)](http://venturebeat.com/2014/11/24/choosing-the-right-development-methodology-is-part-education-part-instinct/)

* [ ] [Operating Systems and Infrastructure Mechanisms](https://drive.google.com/a/gitlab.com/file/d/0B41DBToSSIG_OVYxVFJDOGI3Vzg/view?usp=sharing)
      recorded date: 2015-10-01

* [ ] GitLab (2:06 hours)
  * [ ] [Basic features of GitLab](https://www.youtube.com/watch?v=LgY8XuoM8aU&list=PLLnpHn493BHGgDmJGfCzRYRkFYWcRrxDT&index=3) (10 minutes)
  * [ ] [Create SSH key] (https://www.youtube.com/watch?v=0z28J0RfaJM&list=PLLnpHn493BHGgDmJGfCzRYRkFYWcRrxDT&index=4) (9 minutes)
  * [ ] [Repositories, Projects and Groups](https://www.youtube.com/watch?v=4TWfh1aKHHw&index=1&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e) (41 minutes)
  * [ ] [Commits, Merge requests](https://www.youtube.com/watch?v=Ddd3dbl4-2w&list=PLLnpHn493BHGgDmJGfCzRYRkFYWcRrxDT&index=7) (6 minutes)
  * [ ] [Issue Tracking, Milestones](https://www.youtube.com/watch?v=tv4UM1ruQRs&index=5&list=PLLnpHn493BHGgDmJGfCzRYRkFYWcRrxDT) (9 minutes)
  * [ ] [Creating a GitLab DigitalOcean droplet](https://www.youtube.com/watch?v=QCZl0eNzMTs) (10 minutes)
  * [ ] [GitLab workflow](https://www.youtube.com/watch?v=enMumwvLAug) - Full recap of all of the above (41 minutes)

* [ ] CI:
  * [ ] [Continuous Integration, Delivery and Deployment](https://codeship.com/continuous-integration-essentials)
  * [ ] [Overview](https://www.youtube.com/watch?v=Hs8LCilGVaM) (49 minutes)
  * [ ] [What is a Runner](http://docs.gitlab.com/ce/ci/runners/README.html)?
  * [ ] [What is autoscaling?](https://www.techopedia.com/definition/29432/auto-scaling)
  * [ ] [Documentation](http://docs.gitlab.com/ce/ci/)


* [ ] [Making GitLab Great for Everyone, our response to the Dear GitHub letter](https://www.youtube.com/watch?v=GGC40y4vMx0)

**Stage 1 Tests**

* [ ] [Version Control Systems Test](http://goo.gl/forms/8H8SNcH70T)
* [ ] [Intro to Git Test](http://goo.gl/forms/GgWF1T5Ceg)
* [ ] Create a group on GitLab.com named: "glu_yourname"
* [ ] Add your direct manager and Chad Malchow as master to the group
* [ ] Create a project "About Me" with a README.md with a list of things that we should know about you
* [ ] Create a project "90 day plan" with a README.md with what you plan to do in your first 90 days
* [ ] Create a MR adding something funny to either of the projects and assign to your manager and Chad Malchow to it
* [ ] Create an issue named: "Add me to TrainTool" and assign to Chad Malchow and tag your direct manager

### Stage 2

**Experience first hand the power of GitLab**

* [ ] [Compare GitLab versions](/features/#compare)

* [ ] [GitLab compared to other tools](/comparison/)
    - [Compared to Atlassian (Recording 2016-03-03) ](https://youtu.be/Nbzp1t45ERo)

* [ ] [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq)

* [ ] Understanding DevOps?
    - [Understanding DevOps](https://youtu.be/HpZBnc07q9o)
    - [DevOps at IBM](https://www.youtube.com/user/IBMRational)
    - [DevOps Where To Start](https://www.youtube.com/watch?v=CSrKwP1QrjE)
    - [Agile & DevOps](https://www.youtube.com/watch?v=WqoVeGFjK9k)
    - [Problem Solving with DevOps](https://www.youtube.com/watch?v=pTq9hFBWPeM)

* [ ] JIRA and Jenkins integrations in GitLab
    - [Demo of Jira integration within GitLab](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=44b548147a67ab4d8a62274047146415).
      Download [WebEx](https://www.webex.com/play-webex-recording.html) to view this video

* [ ] GitLab Flow
    - [Recording of what it is](https://www.youtube.com/watch?v=UGotqAUACZA)
    - [Intro to GitLab Workflow](https://www.youtube.com/watch?v=enMumwvLAug)
    - [GitLab Flow blog post](https://about.gitlab.com/2014/09/29/gitlab-flow/)
    - [GitLab Flow documentation](http://doc.gitlab.com/ee/workflow/gitlab_flow.html)

* [ ] GitLab Integrations
    - Supporting: [Documentation on Integrating Jira with GitLab](http://doc.gitlab.com/ee/integration/jira.html) and [Demo of Jira integration within GitLab](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=44b548147a67ab4d8a62274047146415).  Download [WebEx](https://www.webex.com/play-webex-recording.html) to view this video
    - Supporting: [Atlassian Crowd feature request](http://feedback.gitlab.com/forums/176466-general/suggestions/4324384-integration-with-crowd)
    - Supporting: [Documentation on Integrating Jenkins with GitLab](http://doc.gitlab.com/ee/integration/jenkins.html)
    - Supporting: [Documentation on Integrating Bamboo with GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/project_services/bamboo.md)
    - Supporting: [Documentation on Integrating Slack with GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/integration/slack.md)

* [ ] Migrating to GitLab from SVN/GitHub Enterprise/BitBucket/Perforce
    - Supporting: [Migrating from BitBucket/Stash](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_bitbucket.html)
    - Supporting: [Migrating from GitHub](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_github.html)
    - Supporting: [Migrating from SVN](http://doc.gitlab.com/ee/workflow/importing/migrating_from_svn.html)
    - Supporting: [Migrating from Fogbugz](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_fogbugz.html)


**Stage 2 Tests**

* [ ] [Ecosystem Test](http://goo.gl/forms/5Vrf3CE0iC)
* [ ] Complete TrainTool Course - GitLab vs. The Competition
* [ ] Complete TrainTool Course - Jira Support
* [ ] Complete TrainTool Course - Selling EE to CE User
* [ ] Complete TrainTool Course - On-Premised vs. SaaS

### Stage 3

**Get familiar with GitLab's advanced features**

* [ ] [Compare GitLab versions](https://about.gitlab.com/features/#compare)

* [ ] Scalability and High Availability
    - [Recording 2015-12-03](https://www.youtube.com/watch?v=cXRMJJb6sp4&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=2)
    - [High Availability Documentation](https://about.gitlab.com/high-availability/)
    - [High Availability AWS Tutorial](https://www.youtube.com/watch?v=57qIiunuhaQ)

* [ ] Managing LDAP, Active Directory
    - [Recording of what it is and how to set it up](https://www.youtube.com/watch?v=HPMjM-14qa8)

* [ ] Managing Permissions within EE
    - [Recording of what it is and how to set it up](https://www.youtube.com/watch?v=DjUoIrkiNuM)

* [ ] GitLab 8.2
    - [Recording 2015-11-19](https://www.youtube.com/watch?v=09RLHyMFfpA&index=3&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e)
    - [Slides](https://gitlab.com/gitlab-org/University/blob/master/classes/8.2.markdown) 2015-11-19

* [ ] Upcoming in EE and Big files in Git (Git LFS, Annex)
    - [Upcoming in EE](https://gitlab.com/gitlab-org/University/blob/master/classes/upcoming_in_ee.md)
    - [Big files in Git (Git LFS, Annex)](https://gitlab.com/gitlab-org/University/blob/master/classes/git_lfs_and_annex.md)

* [ ] Familiarize yourself with our [product direction](https://about.gitlab.com/direction/).  Pay special attention to new features for EE and EE Options.  Each feature that is hyperlinked will take you to the issue to learn more.  

* [ ] [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

* [ ] [Client Demo of GitLab with Job and Haydn](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=ae7b72c61347030e8aa75328ed4b8660)

* [ ] [Idea to Production Video}(https://www.youtube.com/watch?v=pY4IbEXxxGY&feature=em-subs_digest)

* [ ] Step-by-step demo script(https://about.gitlab.com/handbook/sales/demo/)

* [ ] [Sales Onboarding materials](/handbook/sales-onboarding/)

**Stage 3 tests**

* [ ] [GitLab 8.2 Test](http://goo.gl/forms/9PnmhiNzEa)
* [ ] [Big Files in Git Test](http://goo.gl/forms/RFsNK9fKuj)
* [ ] Complete TrainTool Course - Asking Effective Questions
* [ ] Complete TrainTool Course - Closing Techniques
* [ ] Complete TrainTool Course - Handling Pricing Objections
* [ ] Record a demo, referencing the [demo script](https://about.gitlab.com/handbook/sales/demo/), and using either Blue Jeans or WebEx. Send to your manager and Chad Malchow.  Demo scenario is an organization with 500 seats who uses Jira and BitBucket for a few teams, subversion within a couple teams and GitLab CE is also installed.
* [ ] Within this sales bootcamp issue, please submit a comment with a link to an EE Option and the names of salespeople you have seen contribute to the discussion.

### Stage 4 <a name="stage-4"></a>

* [ ] Login to [Salesforce.com](http://www.salesforce.com/), you should receive an email asking you to change your password.  Once you are in Salesforce, please familiarize yourself wth these reports/views as they will be critical in helping you manage your business.
* [ ] Review in order, learning material on our [Sales Handbook](https://about.gitlab.com/handbook/sales/) page.
* [ ] Review [Your Current Month Pipeline](https://na34.salesforce.com/00O61000001uYbM) This view is to focus you on what you are committing to closing this month.  Are you in the right stage? What is needed to advance the sale to the next stage?
* [ ] Review [Your Total Pipeline](https://na34.salesforce.com/00O61000001uYbR) This view should be used to identify where you are going and where you need focus to ensure you are successful.  What needs to to close and/or where you need to build up your pipleine - new business, expansion, add-on
* [ ] [Your Leads](https://na34.salesforce.com/00Q?fcf=00B610000027qT9&rolodexIndex=-1&page=1) This view should be used to make sure you are followign up on each lead in a timely manner and have a plan on how you qualify or disqualify a lead.
* [ ] Review [Your Personal Dashboard](https://na34.salesforce.com/01Z61000000J0gx) This dashboard should be used to understand where you have been, where you are at, where are you going and do you have the pipeline to get to where you need to be.
* [ ] [Accounts you Own](https://na34.salesforce.com/001?fcf=00B61000001XPLz) This view is to be used to identify expansion opportunities, who you have neglected and the mix of customers to prospects you are working


**Stage 4 test**
* [ ] Create Fake Lead in Salesforce.  Lead First Name - Test; Lead Last Name - GitLab Bootcamp.  Company: GitLab Sales Bootcamp. Lead Source: Other
* [ ] Convert Lead into an opportunity and Account
* [ ] Ensure Opportunity follows naming convention process
* [ ] Add Opportunity Product to Opportunity.  Quantity 2 of 100 seat Standard, one year.
* [ ] Change close date to be 90 days out
* [ ] Opportunity Stage to presenting solution

```


#### Supporting Information

1. [Getting Technical Support](https://about.gitlab.com/handbook/support/#internal)

1. [Our Sales Handbook](https://about.gitlab.com/handbook/sales/)

1. [Sales Best Practices Training](https://about.gitlab.com/handbook/sales-training/)

1. [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit)

1. [Recorded Practice Sales Demos](https://drive.google.com/drive/u/0/folders/0B_XVovPbWgADM1M3VUg1ZVJ0UjQ)
